const Discord = require('discord.js');
const SuperBot = require('./superBot');

const settings = require('./settings.json');
const dataUtils = require('./verification/dataUtils');
const oldDataUtils = require('./verification/oldDataUtils');
const messageUtils = require('./messageUtils');

const botChannelID = '672514494903222311';

/**
 * Does something.
 * @param {SuperBot} bot The bot that does something.
 */
async function doSomething(bot) {
	let lastMessage = null;
	let oldDataChannel = bot.textChannels.get(settings.dataChannelIDs.sorbonne);
	console.log(oldDataChannel.name);
	let dataMessages = await messageUtils.getMessages(oldDataChannel, null, 50);
	while (dataMessages.length > 0) {
		let dataObjs = dataMessages.map(message => {
			let data = oldDataUtils.codeToJSON(message.content);
			delete data.username;
			delete data.joinedAt;

			let m = bot.mainGuild.members.cache.get(data.id);
			if (m) data = {discordTag: `${m.user.username}#${m.user.discriminator}`, ...data};
			else data = {discordTag: undefined, ...data};
			delete m;
			return data;
		})

		for(let dataObj of dataObjs) {
			await bot.databaseChannel.send(dataUtils.JSONToCode(dataObj)).catch();
			for (let i = 0; i<50000000; i+=0.1) {};
		}
		for (let i = 0; i<60000000; i+=0.1) {};
		
		lastMessage = dataMessages[dataMessages.length-1].id;
		dataMessages = await messageUtils.getMessages(oldDataChannel, lastMessage, 50);
		delete dataObjs;
	}
	return Promise.resolve();
}

module.exports = doSomething;