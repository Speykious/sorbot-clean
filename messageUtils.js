const Discord = require('discord.js');

/**
 * Récupère les messages dans un salon.
 * @param {Discord.TextChannel} channel Le salon servant de base de données.
 * @param {number} msglimit Le nombre maximum de messages à récupérer.
 * @returns {Promise<Discord.Message[]>} Le tableau des messages récupérés.
 */
async function getMessages(channel, before = null, msglimit = 30) {
	let messages = await channel.messages
		.fetch({limit: msglimit, before: before})
		.catch(e => console.log(`Could not fetch messages :( => ${e}`));
	
	return [...messages.values()];
}

/**
 * Retourne le message le plus récent correspondant au critère du finder.
 * @param {Discord.TextChannel} channel Le salon dans lequel trouver le message.
 * @param {(message: Discord.Message) => boolean} finder La fonction définissant le critère pour trouver le message.
 */
async function findMessage(channel, finder) {
	let messages = await getMessages(channel);

	while (messages.length > 0) {
		for (let message of messages) {
			if (finder(message))
				return Promise.resolve(message);
		}
		
		messages = await getMessages(channel, messages.reverse()[0].id);
	}

	return Promise.reject('Message not found');
}

/**
 * Retourne le message le plus récent correspondant au critère du finder. Renvoie null sinon.
 * @param {Discord.TextChannel} channel Le salon dans lequel trouver le message.
 * @param {(message: Discord.Message) => boolean} finder La fonction définissant le critère pour trouver le message.
 */
async function findMessageWithoutError(channel, finder) {
	try {
		return await findMessage(channel, finder);
	} catch {
		return Promise.resolve(null);
	}
}



/**
 * Maps all the messages of a channel into a single array.
 * @param {Discord.TextChannel} channel The channel to map messages from.
 * @param {(message: Discord.Message) => any} mapper The mapper that transforms the messages.
 */
async function mapAllMessages(channel, mapper) {
	let mappedMessages = [];
	let someMessages = await getMessages(channel);

	while (someMessages.length > 0) {
		mappedMessages = [...mappedMessages, ...someMessages.map(mapper)];
		someMessages = await getMessages(channel, someMessages.reverse()[0].id);
	}

	return Promise.resolve(mappedMessages);
}

module.exports = {
	getMessages,
	findMessage,
	findMessageWithoutError,
	mapAllMessages
}