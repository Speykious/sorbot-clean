const Discord = require('discord.js');

const { version } = require('./package.json');
const settings = require('./settings.json');
require('dotenv-flow').config();
const SuperBot = require('./superBot');
const fs = require('fs');
const GDPR = fs.readFileSync('./messages/RGPD.md', {encoding: 'UTF8'});

// Le booléen est le maintenanceMode.
const bot = new SuperBot(!!process.env.MAINTENANCE_MODE);
console.log(bot.maintenanceMode);


const manageDmMessage = require('./manageDmMessage');
const manageTextMessage = require('./manageTextMessage');
const messageUtils = require('./messageUtils');
const dataUtils = require('./verification/dataUtils');
const colors = require('./colors');
// const convertDatabase = require('./convertDatabase');


bot.on('ready', () => {
	console.log(`Bot lancé et opérationnel. [${bot.user.username} v${version}]`);

	bot.user.setPresence({
		activity: {
			type: 'PLAYING',
			name: 'with discord.js v12 💻',
			url: 'https://discord.js.org/',
		}
	}).then(async presence => {
		
		console.log('Statut établi :)');

		// console.log('Destroying myself...');
		// bot.destroy();
	}).catch(e => {
		console.log(`ERROR -> ${e}`);
		bot.destroy();
	});
})


bot.on('guildMemberAdd', member => {
	bot.welcome(member);
	console.log(`${member.user.username}#${member.user.discriminator} (${member.id}) vient d'arriver sur le serv <_<`);
})

bot.on('guildMemberRemove', member => {
	bot.goodbye(member);
})

bot.on('guildMemberUpdate', (oldMember, newMember) => {
	let levelRoles = Object.values(settings.roleIDs.levels);
	if (levelRoles.some(snowflake => newMember.roles.cache.has(snowflake)))
		newMember.roles.remove(settings.roleIDs.undecided);
})

bot.on('message', msg => {
	switch (msg.channel.type) {
		case 'text':
			manageTextMessage(bot, msg).catch();
			break;
		
		case 'dm':
			manageDmMessage(bot, msg).catch();
			break;
	}
})

// Attention : c'est le bot 'SorBOT Local'.
if (bot.maintenanceMode)
	bot.login(process.env.LOCAL_TOKEN);
else bot.login(process.env.TOKEN);