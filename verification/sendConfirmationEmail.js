const nodemailer = require('nodemailer');
const botmail = process.env.EMAIL_ADDR;
const botpass = process.env.EMAIL_PASS;

const Discord = require('discord.js');

const fs = require('fs');
const code_de_confirmation = fs.readFileSync(__dirname + '/code_de_confirmation.html', {encoding: 'utf8'});
const mailEnvoye = fs.readFileSync(__dirname + '/../messages/mailEnvoye.md', {encoding: 'utf8'});

/**
 * Génère un code de caractères aléatoires.
 * @param {number} size Le nombre de caractères du code.
 * @param {string} alpha Les caractères disponibles du code.
 */
function generateCode(size, alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
	let code = '';
	for (let i = 0; i < size; i++)
		code += alpha[Math.floor(Math.random()*alpha.length-1e-6)];
	
	return code;
}

const transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: botmail,
		pass: botpass
	}
});

/**
 * Envoie un mail de confirmation à l'adresse mail 'address'
 * et renvoie le code de confirmation envoyé,
 * ainsi qu'un message décrivant le résultat.
 * @param {string} address L'adresse mail à laquelle envoyer le message de confirmation.
 * @param {Discord.User} author L'auteur du message d'origine.
 * @return {Promise<string>} La Promise du code de confirmation envoyé.
 */
async function sendConfirmationEmail(address, author) {
	let code = generateCode(6);

	let mailOptions = {
		from: botmail,
		to: address,
		subject: 'Code de confirmation',
		text: `Bonjour ${author.username}#${author.discriminator} (${author.id}),\nRenvoyez au bot un message privé DANS DISCORD contenant uniquement le code suivant :\n\n${code}`,
		html: code_de_confirmation
			.replace('${code}', code)
			.replace('${discordTag}', `${author.username}#${author.discriminator}`)
			.replace('${id}', author.id)
	};

	return new Promise((resolve, reject) => {
		transporter.sendMail(mailOptions,
			err => {
				if (err) {
					author.send('**Erreur :** le mail n\'a pas pu être envoyé.').catch();
					console.log("\x1b[1mFATAL TRANSPORTER SEND-MAIL ERROR:\x1b[0m", err);
					reject(err)
				} else {
					author.send(mailEnvoye.replace('${email}', address)).catch();
					resolve(code)
				}
			});
	})
}

module.exports = sendConfirmationEmail;
