const Discord = require('discord.js');
const settings = require('../settings.json');
const Email = require('./Email');
const messageUtils = require('../messageUtils');
const dataUtils = require('./dataUtils');

/**
 * S'occupe de vérifier une adresse Email.
 * @param {string} address L'adresse Email dont la fonction s'occupe.
 * @param {Discord.User} author L'auteur du message d'origine.
 */
async function verifyEmail(bot, address, author) {
	try {
		console.log(address);
		let emailObj = await Email.parseEmail(address);
		console.log(address);

		let validDomains = [
			...settings.studentValidDomains,
			...settings.PHDValidDomains
		];

		if (!validDomains.some(domain => domain === emailObj.domain))
			throw 'Ceci n\'est pas une adresse mail valide.';
		
		author.send('Donnez-nous un instant pendant que l\'on vérifie une éventuelle présence de l\'adresse dans la base de donnée...');
		// chercher l'email dans la base de donnée
		let dataMessage = await messageUtils.findMessageWithoutError(bot.databaseChannel,
			message => {
				return dataUtils.codeToJSON(message.content).email === address;
			}
		);

		if (dataMessage) throw 'Cette adresse a déjà été utilisée.';
		return Promise.resolve(address);

	} catch (err) {
		if (err != 'Message not found') {
			author.send(`**Erreur :** ${err}`).catch();
			return Promise.reject(err);
		} else {
			author.send(`**Erreur alternative:** ${err}`).catch();
		}
	}
}

module.exports = verifyEmail;
