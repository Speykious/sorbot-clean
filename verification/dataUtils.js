const Discord = require('discord.js');

/**
 * Gets the JSON code block from an object.
 * @param {any} object 
 */
function JSONToCode(object) {
	let code = JSON.stringify(object, null, '\0');
	return '```json\n' + code.substring(2, code.length-2) + '\n```';
}

/**
 * Gets the json object from a message's content.
 * @param {string} str The content to get jsonCode from.
 */
function codeToJSON(str) {
	try {
		return JSON.parse(`{\n${str.substring(8, str.length-3)}\n}`);
	} catch (e) {
		return {ERROR: e};
	}
}

/**
 * Sends JSON data as code in the channel.
 * @param {Discord.TextChannel} channel 
 * @param {any} object 
 */
function sendJSON(channel, object) {
	channel.send(JSONToCode(object)).catch();
}

module.exports = {
	codeToJSON,
	JSONToCode,
	sendJSON
}