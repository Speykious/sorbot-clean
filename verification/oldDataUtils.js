const Discord = require('discord.js');

/**
 * Gets the JSON code block from an object.
 * @param {any} object 
 */
function JSONToCode(object) {
	return '```json\n'
		+ JSON.stringify(object, null, '  ')
		+ '\n```';
}

/**
 * Sends JSON data as code in the channel.
 * @param {Discord.TextChannel} channel 
 * @param {any} object 
 */
function sendJSON(channel, object) {
	channel.send(JSONToCode(object)).catch();
}

/**
 * Gets the json object from a message's content.
 * @param {string} str The content to get jsonCode from.
 */
function codeToJSON(str) {
	try {
		return JSON.parse(str.substring(8, str.length-3));
	} catch (e) {
		return {ERROR: e};
	}
}

module.exports = {
	codeToJSON,
	JSONToCode,
	sendJSON
}