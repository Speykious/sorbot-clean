/**
 * Une classe pour les adresses Email. Était-ce vraiment nécessaire ? <_<
 * @property {string} username Le nom d'utilisateur de l'adresse Email.
 * @property {string} domain Le nom de domaine de l'adresse Email.
 */
class Email {
	static regex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

	/**
	 * Crée un objet Email en utilisant une adresse.
	 * @param {string} address 
	 */
	constructor(address) {
		let sep = address.indexOf('@');
		this.username = address.substring(0, sep);
		this.domain = address.substring(sep);
	}

	/**
	 * Renvoie l'adresse Email sous la forme `username@domain`.
	 */
	get address() {
		return this.username + this.domain;
	}

	/**
	 * Parse une string en objet Email.
	 * @param {string} address 
	 */
	static async parseEmail(address) {
		if (Email.regex.test(address)) {
			return Promise.resolve(new Email(address));
		} else {
			return Promise.reject('Ceci n\'est pas une adresse mail.');
		}
	}
}

module.exports = Email;