const Discord = require('discord.js');
const SuperBot = require('../superBot');
const settings = require('../settings.json');
const Email = require('./Email');
const sendConfirmationEmail = require('./sendConfirmationEmail');
const fs = require('fs');
const vousEtesVerifie = fs.readFileSync(__dirname + '/../messages/vousEtesVerifie.md', {encoding: 'utf8'});

/**
 * Vérifie le code de confirmation.
 * @param {Discord.Message} msg Le message d'origine.
 * @param {{discordTag: string, id: string, email: string, code: string}} data Les données de l'utilisateur.
 */
async function verifyCode(msg, data) {
	if (msg.content === data.code) {
		msg.author.send(vousEtesVerifie).catch();
		return Promise.resolve();
	} else return Promise.reject('Le code n\'est pas le bon. Réessayez.');
}

/**
 * Assigne des rôles au membre.
 * @param {Discord.GuildMember | Discord.PartialGuildMember} member Le membre se voyant attribué un rôle.
 * @param {{discordTag: string, id: string, email: string, code: string}} data Les données de l'utilisateur.
 */
async function assignRoles(member, data) {
	try {
		let emailObj = await Email.parseEmail(data.email);

		await member.roles.remove(settings.roleIDs.unverified);

		await member.roles.add(settings.roleIDs.verified);
		await member.roles.add(settings.roleIDs.undecided);
		
		if (settings.PHDValidDomains
			.some(domain => domain === emailObj.domain)) {
			await member.roles.add(settings.roleIDs.levels.professor);
		}

		return Promise.resolve(member);
	} catch (err) {
		console.log('Oh shit. ' + err);
		return Promise.reject(err);
	}
}

/**
 * S'occupe des opérations qui suivent celles de la vérification d'adresse.
 * @param {SuperBot} bot Le bot s'occupant des opérations.
 * @param {Discord.Message} msg Le message d'origine.
 * @param {{discordTag: string, id: string, email: string, code: string}} data Les données de l'utilisateur.
 */
async function afterEmail(bot, msg, data) {
	try {
		switch (msg.content) {
			case 'again':
				data.code = await sendConfirmationEmail(data.email, msg.author);
				break;
			
			case 'change':
				delete data.email;
				delete data.code;
				msg.author.send('Votre mail a été supprimé de la base de donnée, vous pouvez en entrer un à nouveau.').catch();
				break;
	
			default:
				await verifyCode(msg, data);
				await assignRoles(bot.mainGuild.members.cache.get(msg.author.id), data);
				delete data.code;
				break;
		}
	
		return Promise.resolve(data);
	} catch (err) {
		msg.author.send(`**Erreur :** ${err}`).catch();
		return Promise.reject(err);
	}
}

module.exports = afterEmail;