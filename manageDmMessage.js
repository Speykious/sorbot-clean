const Discord = require('discord.js');
const SuperBot = require('./superBot');

const fs = require('fs');
let notInArchives = fs.readFileSync(__dirname + '/messages/notInArchives.md', {encoding: 'utf8'});

const messageUtils = require('./messageUtils');
const dataUtils = require('./verification/dataUtils');
const oldDataUtils = require('./verification/oldDataUtils');

const verifyEmail = require('./verification/verifyEmail');
const sendConfirmationEmail = require('./verification/sendConfirmationEmail');
const afterEmail = require('./verification/afterEmail');

/**Gère les messages venant de discussions privées.
 * @param {SuperBot} bot Le bot utilisé pour gérer les messages.
 * @param {Discord.Message} msg Le message à gérer.
 */
async function manageDmMessage(bot, msg) {
	try {
		let member = bot.mainGuild.members.cache.get(msg.author.id);
		if (bot.maintenance(member)) {
			msg.author.send('Désolé, le bot est actuellement en mode maintenance.').catch();
			return;
		}
	
		// Chercher l'utilisateur dans la base de données
		let dataMessage = await messageUtils.findMessage(bot.databaseChannel,
			message => dataUtils.codeToJSON(message.content).id === msg.author.id
		);
		
		// Si le message dans la base de données vient de SorBOT Local, on le supprime pour le remettre
		// '689806945523466284' est l'id de SorBOT Local.
		if (dataMessage.author.id === '689806945523466284') {
			let preciousData = dataMessage.content;
			dataMessage.delete();
			dataMessage = await bot.databaseChannel.send(preciousData).catch();
			console.log(dataMessage.content);
		}
		
		let data = dataUtils.codeToJSON(dataMessage.content);
		if (!data.email) {
			data.email = await verifyEmail(bot, msg.content, msg.author);
			data.code = await sendConfirmationEmail(msg.content, msg.author);
			dataMessage.edit(dataUtils.JSONToCode(data));
		} else if (data.code) {
			data = await afterEmail(bot, msg, data);
			dataMessage.edit(dataUtils.JSONToCode(data));
		} else {
			// Utilisation à méditer 🤔
			msg.author.send('Vous êtes vérifié. Pourquoi m\'envoyez-vous un message ?').catch();
		}

	} catch (err) {
		switch (err) {
			case 'Message not found':
				msg.author.send(notInArchives).catch();
				break;

			default:
				console.trace('Wait wtf -> ' + err);
				break;
		}
	}	
}

module.exports = manageDmMessage;