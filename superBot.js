const Discord = require('discord.js');

const fs = require('fs');
let bienvenue = fs.readFileSync(__dirname + '/messages/bienvenue.md', {encoding: 'utf8'});
let policonf = fs.readFileSync(__dirname + '/messages/politique_conf.md', {encoding: 'utf8'});
let byebyes = fs.readFileSync(__dirname + '/messages/byebye.md', {encoding: 'utf8'}).split('\n');
let blByebyes = fs.readFileSync(__dirname + '/messages/byebyeBorderline.md', {encoding: 'utf8'}).split('\n');

const { sendJSON } = require('./verification/dataUtils');
const messageUtils = require('./messageUtils');
const dataUtils = require('./verification/dataUtils');
const settings = require('./settings.json');

/**
 * Récupère le Discord Tag de l'utilisateur, sous la forme `username#1234`.
 * @param {Discord.User} user L'utilisateur donc on récupère le Discord Tag.
 */
function getDiscordTag(user) {
	return `${user.username}#${user.discriminator}`;
}

class SuperBot extends Discord.Client {
	/**Crée un objet SuperBot.
	 * @param {any} settings Les options du bot.
	 * @param {boolean} maintenanceMode (Dés)Activation du mode maintenance du bot.
	 * @param {Discord.ClientOptions} options The client options.
	 */
	constructor(maintenanceMode = false, options = null) {
		if (options) super(options);
		else super();

		this.maintenanceMode = maintenanceMode;
	}

	/**
	 * @param {Discord.GuildMember} member Membre utilisé pour déterminer l'état individuel de maintenance.
	 * @returns {boolean} Propriété utilisée pour toutes les portions de code dépendantes du mode de maintenance.
	 */
	maintenance(member) {
		return this.maintenanceMode && !settings.testers.some(snowflake => {
			// console.log(`Testing ${snowflake}: ${member.id === snowflake}`);
			return member.id === snowflake;
		});
	}

	/**
	 * @returns {Discord.Collection<string, Discord.TextChannel>} Collection de toutes les chaînes textuelles du bot.
	 */
	get textChannels() {
		return this.channels.cache
			.filter(chan => chan.type === 'text');
	}
	
	/**
	 * @returns {Discord.Collection<string, Discord.DMChannel>} Collection de toutes les chaînes dm du bot.
	 */
	get dmChannels() {
		return this.channels.cache
			.filter(chan => chan.type === 'dm');
	}

	/**
	 * @returns {Discord.TextChannel} Le salon servant de base de données.
	 */
	get databaseChannel() {
		return this.textChannels
			.get(settings.dataChannelIDs.sorbonne);
	}

	/**
	 * Le serveur principal du bot.
	 */
	get mainGuild() {
		return this.guilds.cache
			.get(settings.serverIDs.sorbonne);
	}

	/**Sends some content in a channel.
	 * @param {Discord.Client} bot The client that sends the content.
	 * @param {string} channelID The ID of the channel where we send the content.
	 * @param {any} content The content that is sent.
	 */
	async send(channelID, content) {
		return this.textChannels.get(channelID)
			.send(content)
			.catch(e => console.trace(`Shit: send fucked up :( -> ${e}`));
	}

	/**Envoie du contenu dans un salon.
	 * @param {Discord.Client} bot Le Client qui envoie le contenu dans le salon.
	 * @param {string} channelID L'ID du salon dans lequel on envoie le contenu.
	 * @param {any} content The content that is sent.
	 */
	async sendTo(userID, content) {
		return this.users.cache.get(userID)
			.send(content)
			.catch(e => console.trace(`Shit: sendTo fucked up :( -> ${e}`));
	}

	/**Détermine si le membre a un certain rôle.
	 * @param {Discord.GuildMember | Discord.PartialGuildMember} member Le membre.
	 * @param {string} roleID L'ID du rôle du membre.
	 */
	hasRole(member, roleID) {
		return member.roles.cache.some(role => role.id === roleID);
	}

	/**
 * Accueille un membre.
 * @param {Discord.GuildMember | Discord.PartialGuildMember} member Le membre à accueillir.
 */
	welcome(member) {
		member.roles.add(settings.roleIDs.unverified)
			.catch(e => console.trace(`Shit: roles.add fucked up :( -> ${e}`));
		
		sendJSON(this.databaseChannel, {
			discordTag: getDiscordTag(member.user),
			id: member.id
		});

		// Portion basse du code ignorée en mode maintenance
		if (this.maintenance(member)) return;
		
		member.send(bienvenue
			.replace('${PHDValidDomains}',
				settings.PHDValidDomains
				.map(domain => '- ' + domain)
				.join('\n')
			)).then(() => {
				member.send(policonf).catch();
			}).catch(e => console.log(`Shit: bienvenue fucked up :( -> ${e}`));
	}
	/**
	 * Dit au revoir à un membre.
	 * @param {Discord.GuildMember} member Membre qui est parti.
	 */
	goodbye(member) {
		// Enlever l'utilisateur de la base de données
		messageUtils.findMessage(this.databaseChannel,
			message => dataUtils.codeToJSON(message.content).id === member.id
		).then(message => message.delete());
		
		let byebyeChoices = [...byebyes];
		if (!member.roles.cache.has(settings.roleIDs.levels.professor))
			byebyeChoices = [...byebyeChoices, ...blByebyes];
		let byebyeMessage = byebyeChoices[Math.floor(Math.random()*byebyes.length-1e-6)];
		byebyeMessage = byebyeMessage.replace('${NAME}', member.displayName);
		

		this.textChannels.get(settings.byebyeChannelID)
			.send(byebyeMessage).catch();
			
	}
}

module.exports = SuperBot;