const Discord = require('discord.js');
const SuperBot = require('./superBot');

const settings = require('./settings.json');
const dataUtils = require('./verification/dataUtils');
const messageUtils = require('./messageUtils');

const prefixRegex = new RegExp(`^${settings.prefix}`, 'g');
const mentionRegex = /^<@!\d{18}>\s*/g;
const emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})/;

/**Gère les messages venant de salons textuels.
 * @param {SuperBot} bot Le bot utilisé pour gérer les messages.
 * @param {Discord.Message} msg Le message à gérer.
 */
async function manageTextMessage(bot, msg) {
	try {
		// Vérifier le prefix
		let match = msg.content.match(prefixRegex);
		if (!match) return;		
		let args = msg.content.substring(match[0].length);

		let memberAuthor = bot.mainGuild.members.cache.get(msg.author.id);
		if (bot.maintenance(memberAuthor)) {
			msg.channel.send('Désolé, le bot est actuellement en mode maintenance.').catch();
			return;
		}

		// commande incode
		match = args.match(/^incode\s*/g);
		if (match) {
			args = args.substring(match[0].length);
			msg.channel.send(`Contenu du message : \`${args}\``).catch();
			return;
		}


		// commande say
		match = args.match(/^say\s*/g);
		if (match) {
			args = args.substring(match[0].length);
			match = args.match(/^`.+`$/g);
			if (match) {
				msg.channel.send(`**${msg.author.username}** : ${args.substring(1, args.length-1)}`).catch();
			} else {
				msg.channel.send(`**Erreur :** un bloc de code inline \`comme ceci\` était attendu.`).catch();
			}
			return;
		}



		// Vérifier le nom de la commande verify
		match = args.match(/^verify\s*/g);
		if (!match) throw `**Erreur 404 :** la commande entrée d'existe pas.`;
		args = args.substring(match[0].length);

		// Vérifier les permissions du membre (doit avoir le rôle Admin).
		if (!memberAuthor.roles.cache.some(role => role.id === settings.roleIDs.admin))
			throw `Non mais vous vous prenez pour qui <@!${msg.author.id}> ? Seuls les Admins ont accès à cette commande.`;
		
		// Vérifier le premier argument (mention)
		match = args.match(mentionRegex);
		if (!match) throw `**Erreur syntaxique :** vous n'avez pas entré le paramètre \`<mention>\` correctement.`;
		let ID = args.substring(3, 21);
		args = args.substring(match[0].length);

		// Vérifier le deuxième argument (email)
		match = args.match(emailRegex);
		if (!match) throw `**Erreur syntaxique :** vous n'avez pas entré le paramètre \`<email>\` correctement.`;
		let email = args.substring(0, match[0].length);
		args = args.substring(match[0].length);
		match = args.match(/\s+/g);	// suppression des espaces
		if (match) args = args.substring(match[0].length);
		
		// Vérifier le troisième argument (choix)
		let prof = /^prof$/g.test(args);
		
		// ajouter l'email dans la base de données
		let dataMessage = await messageUtils.findMessage(bot.databaseChannel,
			message => dataUtils.codeToJSON(message.content).id === ID
		);
		let data = dataUtils.codeToJSON(dataMessage.content);
		data.email = email;
		dataMessage.edit(dataUtils.JSONToCode(data));

		let memberTarget = bot.mainGuild.members.cache.get(ID);
		await memberTarget.roles.remove(settings.roleIDs.unverified);
		await memberTarget.roles.add(settings.roleIDs.verified);
		await memberTarget.roles.add(settings.roleIDs.undecided);
		if (prof) await memberTarget.roles.add(settings.roleIDs.levels.professor);

		msg.channel.send(`<@!${ID}> est maintenant vérifié.`).catch();

	} catch (err) {
		switch (err) {
			case 'Message not found':
				msg.channel.send(`**Erreur :** il semblerait que la personne mentionnée ne soit pas dans nos archives.`).catch();
				break;

			default:
				msg.channel.send(err).catch();
				break;
		}
	}
}

module.exports = manageTextMessage;