**Erreur :** Il semblerait que vous ne soyez pas présent dans nos archives.

Il se pourrait que vous ayez rejoint le serveur avant que le bot de sécurité n'ait été achevé.

Dans ce cas, si vous êtes @Non-vérifié, quittez le serveur Sorbonne Jussieu puis rejoignez-le à nouveau.

Si vous êtes @Membre du serveur, alors vous n'avez rien à craindre. Le bot subit au fur et à mesure une amélioration, et il sera bientôt possible de vous enregistrer dans notre base de données sans perdre vos rôles. Si toutefois vous n'avez pas la paresse de réagir pour choisir à nouveau vos rôles, alors vous pouvez aussi quitter puis revenir sur le serveur - **attention**, la vérification requiert votre adresse mail UPMC de domaine `@etu.upmc.fr`.