Mail de confirmation envoyé à l'adresse `${email}`.
Vous pouvez écrire `again` pour renvoyer le mail, ou `change` pour changer le mail.

Attention, il peut prendre une dizaine de minutes à arriver dans votre boîte de réception. Pensez donc à la rafraîchir.
Si au bout de 10 minutes vous ne l'avez pas reçu, consultez bien vos spams, ou encore la page webmail de l'UPMC.
Aussi, Assurez-vous d'avoir renseigné une adresse __***qui existe***__ : nous vous rappelons qu'elles sont généralement de la forme `<prénom>.<nom>@<domaine>`. Allez sur #vérification si après avoir suivi ces démarches, vous n'avez pas reçu le code.