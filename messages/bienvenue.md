Bienvenue sur le serveur __**Sorbonne Jussieu**__. Je suis le ~~bot~~ *automate* du serveur qui s'occupe notamment de la vérification des nouveaux arrivants.

Si vous êtes tout nouveau sur Discord, et que des termes tels que 'salon', 'serveur', 'rôle', 'mention', 'réaction' ou encore 'message privé' vous sont étrangers, cliquez sur le lien ci-dessous qui vous redirigera vers un document PDF de prise en main de Discord, réalisé par nos soins.
-> https://drive.google.com/file/d/1hp_KDQT5rlnQKtOg4wSGmDvxK9ngxaDJ/view?usp=sharing

Pour devenir un membre vérifié, répondez ici en envoyant votre mail UPMC (domaine spécifique, cf. ci-dessous). Nous vous y enverrons un code de confirmation.
**__Si vous êtes étudiant__**, le domaine de votre mail doit être `@etu.upmc.fr` ou `@etu.sorbonne-universite.fr`. Vous devriez en posséder un de la forme `<prénom>.<nom>@etu.upmc.fr`.
Si vous ne connaissez pas votre mail, retournez dans le salon `#aide-mail-upmc` du serveur __**Sorbonne Jussieu**__ dans lequel vous trouverez *d'extrêmement claires instructions* pour l'obtenir et correctement le configurer. Seulement à la suite de cela, vous pourrez revenir dans ce salon de discussion privée pour alors y renseigner votre mail.

**__Si vous êtes professeur et/ou doctorant__**, plusieurs domaines valides sont à votre disposition :
```fix
${PHDValidDomains}
```

Assurez-vous de pouvoir recevoir les messages envoyés à votre boîte mail, que ce soit en passant par celle-ci ou par une redirection automatique configurée vers une adresse personnelle.

Si vous n'êtes pas membre de l'UPMC, mentionnez les @Admin dans le salon #vérification en vue d'une vérification manuelle.