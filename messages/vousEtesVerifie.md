Le code est bon, vous êtes maintenant vérifié.
Si vous êtes étudiant, nous vous avons affecté le rôle @Membre.
Si vous êtes professeur, vous avez en plus le rôle @Professeur.
**Attention :** Pensez à aller dans le salon #rôles pour y prendre les rôles qui correspondent à vos études.
Tant que vous n'en aurez pas choisi, vous aurez le rôle `Indécis`.